package org.docryze.god.note.register;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelId;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldPrepender;
import org.docryze.god.codec.register.RegisterEncoder;
import org.docryze.god.codec.register.RegisterEntry;
import org.docryze.god.net.client.DefaultNetClient;

class RegisterClientTest {

    public static void main(String[] args) throws Exception {
        DefaultNetClient defaultNetClient = new DefaultNetClient(
                "127.0.0.1",
                8080,
                new NioEventLoopGroup(),
                new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        //出站
                        // LengthFieldPrepender是一个编码器，主要是在响应字节数据前面添加字节长度字段
                        ch.pipeline().addLast(new LengthFieldPrepender(2));
                        ch.pipeline().addLast(new RegisterEncoder());
                    }
                });

        ChannelFuture channelFuture = defaultNetClient.getChannelFuture();
        ChannelId id = channelFuture.channel().id();
        System.out.println("id 1:" + id);
        for (int i = 0; i < 10000; i++) {
            channelFuture.channel().writeAndFlush(new RegisterEntry("127.0.0.1",9));
        }


//        ChannelFuture channelFuture2 = defaultNetClient.getChannelFuture();
//        ChannelId id2 = channelFuture.channel().id();
//        System.out.println("id 2:" + id2);
//        for (int i = 0; i < 3; i++) {
//            channelFuture2.channel().writeAndFlush(new RegisterEntry("127.0.0.1",9));
//        }

        defaultNetClient.closeChannelFuture(channelFuture);
        defaultNetClient.shutDown();
    }
}