package org.docryze.god.note.register.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.docryze.god.codec.register.RegisterEntry;

/**
 * @author docryze
 */
public class RegisterHandler extends SimpleChannelInboundHandler<RegisterEntry> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RegisterEntry msg) throws Exception {
        System.out.println("this is channelRead:" + msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.err.println(cause);
        ctx.close();
    }
}
