package org.docryze.god.note.register;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.docryze.god.codec.register.RegisterDecoder;
import org.docryze.god.codec.register.RegisterEncoder;
import org.docryze.god.net.server.DefaultNetServer;
import org.docryze.god.note.register.handler.MonitorChannelHandler;
import org.docryze.god.note.register.handler.RegisterHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author docryze
 */
public class RegisterServer {
    private String host;
    private int port;

    private DefaultNetServer defaultNetServer;

    public RegisterServer(String host, int port) {
        this.host = host;
        this.port = port;

        this.defaultNetServer = new DefaultNetServer(
                this.host,
                this.port,
                new NioEventLoopGroup(),
                new NioEventLoopGroup(),
                new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        //入站
                        // 这里将LengthFieldBasedFrameDecoder添加到pipeline的首位，因为其需要对接收到的数据
                        // 进行长度字段解码，这里也会对数据进行粘包和拆包处理
                        ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(29, 0, 2, 0, 2));
//                        ch.pipeline().addLast(new FixedLengthFrameDecoder(27));
                        ch.pipeline().addLast(new IdleStateHandler(10, 20, 30, TimeUnit.SECONDS));
                        ch.pipeline().addLast(new MonitorChannelHandler());
                        ch.pipeline().addLast(new RegisterDecoder());
                        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));
                        ch.pipeline().addLast(new RegisterHandler());



                        //出站
                        ch.pipeline().addLast(new RegisterEncoder());
                        // LengthFieldPrepender是一个编码器，主要是在响应字节数据前面添加字节长度字段
                        ch.pipeline().addLast(new LengthFieldPrepender(2));

                    }
                });
    }


    public void start() throws Exception{
        this.defaultNetServer.start();
    }

    public void shutdown() throws Exception{
        this.defaultNetServer.shutDown();
    }
}
