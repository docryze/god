package org.docryze.god.net.factory;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;

/**
 * @author docryze
 */
public class ChannelFutureFactory {
    public static ChannelFuture buildChannelFuture(String host, int port, Bootstrap bootstrap) throws Exception{
        ChannelFuture channelFuture = bootstrap.connect(host, port).sync();
        return channelFuture;
    }

    public static ChannelFuture buildChannelFuture(String host, int port, ServerBootstrap serverBootstrap) throws Exception{
        ChannelFuture channelFuture = serverBootstrap.bind(host, port).sync();
        return channelFuture;
    }
}
