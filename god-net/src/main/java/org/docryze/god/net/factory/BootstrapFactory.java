package org.docryze.god.net.factory;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author docryze
 */
public class BootstrapFactory {

    public static Bootstrap buildBootstrap(EventLoopGroup workerGroup, ChannelHandler channelHandler){
        return new Bootstrap()
            .group(workerGroup)
            .channel(NioSocketChannel.class)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .handler(channelHandler);
    }
}
