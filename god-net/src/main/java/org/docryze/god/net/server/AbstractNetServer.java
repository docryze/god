package org.docryze.god.net.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import org.apache.commons.lang3.StringUtils;
import org.docryze.god.net.factory.ServerBootstrapFactory;

/**
 * @author docryze
 */
public abstract class AbstractNetServer implements NetServer {
    private String host;
    private int port;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private ServerBootstrap serverBootstrap;
    private ChannelFuture channelFuture;

    public AbstractNetServer(String host, int port, EventLoopGroup bossGroup, EventLoopGroup workerGroup, ChannelHandler channelHandler) {
        this.host = StringUtils.isBlank(host) ? "127.0.0.1" : host;
        this.port = port;
        this.bossGroup = bossGroup;
        this.workerGroup = workerGroup;

        this.serverBootstrap = ServerBootstrapFactory.buildServerBootstrap(bossGroup,workerGroup,channelHandler);
    }

    public AbstractNetServer(int port, EventLoopGroup bossGroup, EventLoopGroup workerGroup, ChannelHandler channelHandler) {
        this("127.0.0.1",port,bossGroup,workerGroup,channelHandler);
    }

    public ChannelFuture start() throws Exception{
        this.channelFuture = this.serverBootstrap.bind(this.host, this.port).sync();
        return this.channelFuture;
    }


    public void shutDown() throws Exception{
        ChannelFuture closeFuture = this.channelFuture.channel().closeFuture().sync();
        closeFuture.addListener(future -> {
            System.out.println("shutDown closeChannelFuture success ！");
        });

        if(!workerGroup.isShutdown()){
            workerGroup.shutdownGracefully();
            System.out.println("shutDown workerGroup shutdownGracefully success ！");
        }

        if(!bossGroup.isShutdown()){
            bossGroup.shutdownGracefully();
            System.out.println("shutDown bossGroup shutdownGracefully success ！");
        }
    }
}
