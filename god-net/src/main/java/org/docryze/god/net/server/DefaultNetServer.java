package org.docryze.god.net.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;

/**
 * @author docryze
 */
public class DefaultNetServer extends AbstractNetServer{


    public DefaultNetServer(String host, int port, EventLoopGroup bossGroup, EventLoopGroup workerGroup, ChannelHandler channelHandler) {
        super(host, port, bossGroup, workerGroup, channelHandler);
    }

    public DefaultNetServer(int port, EventLoopGroup bossGroup, EventLoopGroup workerGroup, ChannelHandler channelHandler) {
        super(port, bossGroup, workerGroup, channelHandler);
    }
}
