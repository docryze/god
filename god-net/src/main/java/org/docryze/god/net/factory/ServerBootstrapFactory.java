package org.docryze.god.net.factory;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author docryze
 */
public class ServerBootstrapFactory {

    public static ServerBootstrap buildServerBootstrap(EventLoopGroup bossGroup, EventLoopGroup workerGroup, ChannelHandler channelHandler){
        return new ServerBootstrap()
            .group(bossGroup,workerGroup)
            .channel(NioServerSocketChannel.class)
            .childHandler(channelHandler)
            .option(ChannelOption.SO_BACKLOG, 128)
            .childOption(ChannelOption.SO_KEEPALIVE, true);
    }
}
