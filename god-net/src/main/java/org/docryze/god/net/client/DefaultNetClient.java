package org.docryze.god.net.client;

import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;

/**
 * @author docryze
 */
public class DefaultNetClient extends AbstractNetClient{

    public DefaultNetClient(String host, int port, EventLoopGroup workerGroup, ChannelHandler channelHandler) {
        super(host, port, workerGroup, channelHandler);
    }

}
