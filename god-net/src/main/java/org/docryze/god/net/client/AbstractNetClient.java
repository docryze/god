package org.docryze.god.net.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import org.docryze.god.net.factory.BootstrapFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author docryze
 */
public abstract class AbstractNetClient {
    private String host;
    private int port;
    private EventLoopGroup workerGroup;
    private Bootstrap bootstrap;
    private Set<ChannelFuture> channelFutureSet;

    public AbstractNetClient(String host, int port,EventLoopGroup workerGroup , ChannelHandler channelHandler) {
        this.host = host;
        this.port = port;
        this.workerGroup = workerGroup;
        this.channelFutureSet = new HashSet<>();

        this.bootstrap = BootstrapFactory.buildBootstrap(workerGroup, channelHandler);
    }

    public ChannelFuture getChannelFuture() throws Exception{
        ChannelFuture channelFuture = this.bootstrap.connect(this.host, this.port).sync();
        channelFutureSet.add(channelFuture);
        return channelFuture;
    }

    public void closeChannelFuture(ChannelFuture channelFuture) throws Exception{
        ChannelFuture closeFuture = channelFuture.channel().closeFuture();
        closeFuture.addListener(future -> {
            System.out.println("closeChannelFuture success ！");
            channelFutureSet.remove(channelFuture);
        });

    }

    public void shutDown(){
        for(ChannelFuture channelFuture : channelFutureSet){
            ChannelFuture closeFuture = channelFuture.channel().closeFuture();
            closeFuture.addListener(future -> {
                System.out.println("shutDown closeChannelFuture success ！");
                channelFutureSet.remove(channelFuture);
            });
        }
        if(!workerGroup.isShutdown()){
            workerGroup.shutdownGracefully();
            System.out.println("shutDown workerGroup shutdownGracefully success ！");
        }
    }
}
