package org.docryze.god.net.client;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelId;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.CharsetUtil;

class DefaultNetClientTest {

    public static void main(String[] args) throws Exception {
        DefaultNetClient defaultNetClient = new DefaultNetClient(
                "127.0.0.1",
                8080,
                new NioEventLoopGroup(),
                new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new TestClientChannelHandler());
                    }
                });

        ChannelFuture channelFuture = defaultNetClient.getChannelFuture();

        for (int i = 0; i < 3; i++) {
            channelFuture.channel().writeAndFlush(Unpooled.copiedBuffer("this is default net client 1 \n", CharsetUtil.UTF_8));
        }
        ChannelId id = channelFuture.channel().id();
        System.out.println("id 1:" + id);

//        defaultNetClient.closeChannelFuture(channelFuture);


        ChannelFuture channelFuture2 = defaultNetClient.getChannelFuture();

        for (int i = 0; i < 1; i++) {
            channelFuture2.channel().writeAndFlush(Unpooled.copiedBuffer("this is default net client 2 \n", CharsetUtil.UTF_8));
        }

        ChannelId id2 = channelFuture2.channel().id();
        System.out.println("id 2:" + id2);

//        defaultNetClient.closeChannelFuture(channelFuture2);



//        defaultNetClient.shutDown();
    }
}