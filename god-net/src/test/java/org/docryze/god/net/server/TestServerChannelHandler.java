package org.docryze.god.net.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

public class TestServerChannelHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("server: " + ((ByteBuf)msg).toString(CharsetUtil.UTF_8));
//        ctx.channel().writeAndFlush(Unpooled.copiedBuffer("this is server", CharsetUtil.UTF_8));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {

    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state() == IdleState.READER_IDLE) {
//                ctx.writeAndFlush(Unpooled.copiedBuffer("read idle",CharsetUtil.UTF_8));
                System.out.println("read idle");
            } else if (e.state() == IdleState.WRITER_IDLE) {
//                ctx.writeAndFlush(Unpooled.copiedBuffer("write idle",CharsetUtil.UTF_8));
                System.out.println("write idle");
            } else if(e.state() == IdleState.ALL_IDLE){
//                ctx.writeAndFlush(Unpooled.copiedBuffer("all idle",CharsetUtil.UTF_8));
                System.out.println("all idle");
            }
        }
    }
}
