package org.docryze.god.net.server;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

class DefaultNetServerTest {

    public static void main(String[] args) throws Exception {
        DefaultNetServer defaultNetServer = new DefaultNetServer(
                8080,
                new NioEventLoopGroup(),
                new NioEventLoopGroup(),
                new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast(new IdleStateHandler(1,2,3, TimeUnit.SECONDS));
                        socketChannel.pipeline().addLast(new TestServerChannelHandler());
                    }
                });

        ChannelFuture channelFuture = defaultNetServer.start();

        System.out.println("bind success");

        channelFuture.channel().writeAndFlush(Unpooled.copiedBuffer("server write and flush", CharsetUtil.UTF_8));

        System.out.println("writeAndFlush");

        defaultNetServer.shutDown();
    }

}