package org.docryze.god.codec.register;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.CharsetUtil;

/**
 * @author docryze
 */
public class RegisterEncoder extends MessageToByteEncoder<RegisterEntry> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, RegisterEntry registerEntry, ByteBuf byteBuf) throws Exception {
        String registerEntryJson = new Gson().toJson(registerEntry);
        byteBuf.writeBytes(Unpooled.copiedBuffer(registerEntryJson, CharsetUtil.UTF_8));
    }
}
