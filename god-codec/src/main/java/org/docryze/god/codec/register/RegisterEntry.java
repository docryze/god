package org.docryze.god.codec.register;

import java.io.Serializable;

/**
 * @author docryze
 */
public class RegisterEntry implements Serializable {
    private String ip;
    private int port;

    public RegisterEntry() {
    }

    public RegisterEntry(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    @Override
    public String toString() {
        return "RegisterEntry{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
