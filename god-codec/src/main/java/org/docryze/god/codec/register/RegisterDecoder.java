package org.docryze.god.codec.register;

import com.google.gson.Gson;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.util.CharsetUtil;

import java.util.List;

/**
 * @author docryze
 */
public class RegisterDecoder extends ByteToMessageDecoder{

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {

        int readableBytes = byteBuf.readableBytes();
        System.out.println("readableBytes:" + readableBytes);
        byte[] data = new byte[readableBytes];
        byteBuf.readBytes(data);
        String registerEntryJson = new String(data,CharsetUtil.UTF_8);
        // 如果 byteBuf中的数据没有读取完 会重复执行这个方法 需要执行 byteBuf的read操作 而不是直接转换
        // String registerEntryJson = byteBuf.toString(CharsetUtil.UTF_8);
        RegisterEntry registerEntry = new Gson().fromJson(registerEntryJson, RegisterEntry.class);
        System.out.println("list size:" + list.size());
        list.add(registerEntry);
    }
}
